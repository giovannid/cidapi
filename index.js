"use strict";
const restify = require('restify');
const util = require('util');
const sqlite3 = require('sqlite3').verbose();
const fs = require('fs');
const db = new sqlite3.Database('./database.db');

const server = restify.createServer({
    name: 'CIDApi',
    version: '1.0.0'
});

server.pre((req, res, next) => {
    if(req.url.toLocaleLowerCase().indexOf('/', 4) != -1){
        return next(new Error('Solicitação mal formatada!'));
    }
    return next();
});

server.pre((req, res, next) => {
    if( req.url === '/' || req.url.toLocaleLowerCase().includes('v1/') ){
        return next();
    } else {
        return next(new Error('Solicitação mal formatada!'));
    }
});

server.use(restify.throttle({
  burst: 5,
  rate: 1,
  ip: true,
  overrides: {}
}));

server.get('/', (req, res, next) => {

    fs.readFile(__dirname + '/public/index.html', function (err, data) {
        if (err) {
            next(err);
            return;
        }

        res.writeHead(200, {
            'Content-Length': Buffer.byteLength(data),
            'Content-Type': 'text/html' 
        });
        res.end(data);
        return next();
    });
   
});

server.get('/v1/:cid', (req, res, next) => {

    if( req.params.cid !== '' ){

        let cid = req.params.cid.split('.').join('');

        db.serialize(() => {
            db.get('SELECT * FROM cid WHERE cid.codigo = ?', { 1: cid.toLocaleUpperCase() }, (err, row) => {
               if(row !== undefined) {
                   let result = row;
                   result.status = 1;
                   
                   res.setHeader('content-type', 'application/json');
                   res.send(result);
               } else {
                   res.setHeader('content-type', 'application/json');
                   res.send({ "status": 0, "error": "CID não encontrado!" });
               }
            });
        });

        return next();
    } else {
        return next(new Error('Solicitação mal formatada!'));
    }
});

server.listen(process.env.PORT || 5000);